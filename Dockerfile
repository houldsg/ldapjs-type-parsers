FROM node:14-alpine
WORKDIR /home/node/app
ENV npm_config_build_from_source=true
COPY package*.json .
RUN npm i --no-audit
COPY . .
