const tapeTest = require('tape')
const fs = require('fs')
const path = require('path')
const lib = require('./index')


tapeTest('coerceEntry returns the correct data', function(assert){
  assert.plan(1)
  const entry = {
    attributes: [
      {
        type: 'sAMAccountName',
        buffers: [Buffer.from("PICKLERICK", "utf8")],
      },
      {
        type: 'objectSid',
        buffers: [Buffer.from("010500000000000515000000b2b69dc0114797369c4c21ef2e190000", "hex")]
      },
      {
        type: 'objectGUID',
        buffers: [Buffer.from("263e631e1b740e4d94c764d8ded0791f", "hex")]
      },
      {
        type: 'someMultiPartAttribute',
        buffers: [
          Buffer.from("JeffreyWinger", "utf8"),
          Buffer.from("AnnieEdison", "utf8"),
          Buffer.from("TroyBarnes", "utf8"),
          Buffer.from("AbedNadir", "utf8"),
          Buffer.from("UghBrittasInThis?", "utf8"),
        ],
      },
    ],
    object: {
      sAMAccountName: 'PICKLERICK',
      someMultiPartAttribute: [
        "JeffreyWinger",
        "AnnieEdison",
        "TroyBarnes",
        "AbedNadir",
        "UghBrittasInThis?",
      ]
    }
  }
  const coercedEntry = {
    sAMAccountName: 'PICKLERICK',
    objectSid: "S-1-5-21-3231561394-915883793-4011936924-6446",
    objectGUID: "1E633E26-741B-4D0E-94C7-64D8DED0791F",
    someMultiPartAttribute: [
      "JeffreyWinger",
      "AnnieEdison",
      "TroyBarnes",
      "AbedNadir",
      "UghBrittasInThis?",
    ]
  }
  assert.deepEqual(lib.coerceEntry(entry), coercedEntry)
})


tapeTest('parseInteger8 returns the correct Number', function(assert){
  assert.plan(1)
  assert.equal(lib.parseInteger8(Buffer.from("25426", "utf8")), 25426)
})

const certString = fs.readFileSync(path.join(__dirname, 'testData', 'rootca.pem'), { encoding: 'utf8', flag:'r' }).replace(/\r\n/g, '\n')
const buffer = Buffer.from(fs.readFileSync(path.join(__dirname, 'testData', 'rootca.bin'), { encoding: 'binary', flag:'r' }), 'binary')
tapeTest('bufferToPEMCertString returns the correct String', function(assert){
  assert.plan(1)
  assert.equal(lib.bufferToPEMCertString(buffer), certString)
})
tapeTest('PEMCertStringToBuffer returns the correct value', function(assert){
  assert.plan(1)
  assert.equal(lib.PEMCertStringToBuffer(certString).compare(buffer), 0)
})

tapeTest('parseGeneralizedDate returns the correct Date', function(assert){
  assert.plan(1)
  assert.equal(lib.parseGeneralizedDate(Buffer.from("20211225070000.000Z", "utf8")).toISOString(), "2021-12-25T07:00:00.000Z")
})

tapeTest('toGeneralizedDateString returns the correct String', function(assert){
  assert.plan(2)
  assert.equal(lib.toGeneralizedDateString(new Date("2021-12-25T07:00:00.123Z")), "20211225070000.123Z")
  assert.throws(() => lib.toGeneralizedDateString(new Date("ImAnInvalidDate")), TypeError)
})

tapeTest('parseIntegerDate returns the correct Date', function(assert){
  assert.plan(1)
  assert.equal(lib.parseIntegerDate(Buffer.from("132848892000000000", "utf8")).toISOString(), "2021-12-25T07:00:00.000Z")
})

tapeTest('parseLargeInteger returns the correct value', function(assert){
  //this string represents a number larger than Number.MAX_VALUE
  const bigNumberString = "279769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464234321326889464182768467546703537516986049910576551282076245490090389328944075868508455133942304583236903222948165808559332123348274797826204144723168738177180919299881250404026184124858368"
  if(global.BigInt){
    assert.equal(lib.parseLargeInteger(Buffer.from(bigNumberString, "utf8"), global.BigInt), global.BigInt(bigNumberString))
  }
  assert.equal(lib.parseLargeInteger(Buffer.from(bigNumberString, "utf8")), bigNumberString)
  assert.end()
})

tapeTest('parseObjectSID returns the correct value', function(assert){
  assert.plan(1)
  assert.equal(lib.parseObjectSID(Buffer.from("010500000000000515000000b2b69dc0114797369c4c21ef2e190000", "hex")), "S-1-5-21-3231561394-915883793-4011936924-6446")
})

tapeTest('toObjectSIDBuffer returns the correct value', function(assert){
  assert.plan(1)
  assert.equal(lib.toObjectSIDBuffer("S-1-5-21-3231561394-915883793-4011936924-6446").compare(Buffer.from("010500000000000515000000b2b69dc0114797369c4c21ef2e190000", "hex")), 0)
})

tapeTest('parseObjectGUID returns the correct value', function(assert){
  assert.plan(1)
  assert.equal(lib.parseObjectGUID(Buffer.from("263e631e1b740e4d94c764d8ded0791f", "hex")), "1E633E26-741B-4D0E-94C7-64D8DED0791F")
})

tapeTest('toObjectGUIDBuffer returns the correct value', function(assert){
  assert.plan(1)
  assert.equal(lib.toObjectGUIDBuffer("1E633E26-741B-4D0E-94C7-64D8DED0791F").compare(Buffer.from("263e631e1b740e4d94c764d8ded0791f", "hex")), 0)
})

tapeTest('parseBoolean returns the correct values', function(assert){
  assert.plan(4)
  assert.equal(lib.parseBoolean(Buffer.from("TRUE", "utf8")), true)
  assert.equal(lib.parseBoolean(Buffer.from("true", "utf8")), true)
  assert.equal(lib.parseBoolean(Buffer.from("FALSE", "utf8")), false)
  assert.equal(lib.parseBoolean(Buffer.from("0", "utf8")), false)
})

tapeTest('toBooleanString returns the correct value', function(assert){
  assert.plan(4)
  assert.equal(lib.toBooleanString(true), "TRUE")
  assert.equal(lib.toBooleanString(false), "FALSE")
  assert.equal(lib.toBooleanString(0), "FALSE")
  assert.equal(lib.toBooleanString(null), "FALSE")
})

tapeTest('parseUnicodeString returns the correct value', function(assert){
  assert.plan(1)
  assert.equal(lib.parseUnicodeString(Buffer.from("PickleRick", "utf8")), "PickleRick")
})


tapeTest('parseUserAccountControl returns the correct value', function(assert){
  assert.plan(2)
  //a number in binary with 32 bits, all true, converted to a decimal string
  const allTrueNumberString = (0b11111111111111111111111111111111).toString(10)
  //all the boolean properties of the resulting object
  const props = [
    "logonScriptExecuted",
    "accountDisabled",
    "homeDirRequired",
    "lockedOut",
    "passwordNotRequired",
    "passwordCantChange",
    "encryptedPasswordAllowed",
    "duplicateAccount",
    "normalAccount",
    "interdomainTrustAccount",
    "workstationTrustAccount",
    "serverTrustAccount",
    "passwordNeverExpires",
    "msnLogonAccount",
    "smartcardRequired",
    "trustedForDelegation",
    "notDelagated",
    "useDESKeysOnly",
    "dontRequirePreauth",
    "accountExpired",
    "trustedToAuthenticateForDelegation",
  ]
  const allTrueObject = props
    .reduce((acc, prop) => {
      acc[prop] = true
      return acc
    }, {})
  assert.deepEqual(lib.parseUserAccountControl(Buffer.from(allTrueNumberString, "utf8")), allTrueObject)

  const allFalseNumberString = "0"
  const allFalseObject = props
    .reduce((acc, prop) => {
      acc[prop] = false
      return acc
    }, {})
  assert.deepEqual(lib.parseUserAccountControl(Buffer.from(allFalseNumberString, "utf8")), allFalseObject)
})

tapeTest('parseSAMAccountType returns the correct value', function(assert){
  assert.plan(2)
  //a number in binary with 32 bits, all true, converted to a decimal string
  const allTrueNumberString = (0b11111111111111111111111111111111).toString(10)
  //all the boolean properties of the resulting object
  const props = [
    "domainObject",
    "groupObject",
    "nonSecurityGroupObject",
    "aliasObject",
    "nonSecurityAliasObject",
    "userObject",
    "normalUserAccount",
    "machineAccount",
    "trustAccount",
    "appBasicGroup",
    "appQueryGroup",
    "accountTypeMax",
  ]
  const allTrueObject = props
    .reduce((acc, prop) => {
      acc[prop] = true
      return acc
    }, {})
  assert.deepEqual(lib.parseSAMAccountType(Buffer.from(allTrueNumberString, "utf8")), allTrueObject)

  //zero
  const allFalseNumberString = "0"
  const allFalseObject = props
    .reduce((acc, prop) => {
      acc[prop] = false
      return acc
    }, {})
  //the domainObject bitmask is 0x0, I'm not sure what the logic is there but I don't think it can ever be "off" unless I'm doing something wrong...
  allFalseObject.domainObject = true
  assert.deepEqual(lib.parseSAMAccountType(Buffer.from(allFalseNumberString, "utf8")), allFalseObject)
})
