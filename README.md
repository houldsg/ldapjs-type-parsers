## Functions

<dl>
<dt><a href="#coerceEntry">coerceEntry(entry)</a> ⇒ <code>Object</code></dt>
<dd><p>Coerce an ldapjs entry into an object with keys (using entry.object as a base)
(This uses module.attributeTransforms to map attribute names to type parsers. Many of the names present are Active-Directory-LDAP specific and I don&#39;t honestly know how well it works on other instances)</p>
</dd>
<dt><a href="#parseInteger8">parseInteger8(buffer)</a> ⇒ <code>Number</code></dt>
<dd><p>Parse an LDAP Interger8 buffer into a Number</p>
</dd>
<dt><a href="#bufferToPEMCertString">bufferToPEMCertString(buffer)</a> ⇒ <code>String</code></dt>
<dd><p>Parse an LDAP OctetString buffer into a base 64 encoded certificate string with line breaks every 64 characters.
(this should match the content in a .pem certificate file)</p>
</dd>
<dt><a href="#PEMCertStringToBuffer">PEMCertStringToBuffer(str)</a> ⇒ <code>Buffer</code></dt>
<dd><p>Parse a base 64 encoded certificate string into a buffer representing an LDAP OctetString.</p>
</dd>
<dt><a href="#parseGeneralizedDate">parseGeneralizedDate(buffer)</a> ⇒ <code>Date</code></dt>
<dd><p>Parse an LDAP GeneralizedDate buffer into a javascript Date object</p>
</dd>
<dt><a href="#toGeneralizedDateString">toGeneralizedDateString(date)</a> ⇒ <code>String</code></dt>
<dd><p>Convert a javascript Date object into an LDAP GeneralizedDate string
(Can be converted to a buffer with Buffer.from(str, &#39;utf8) but the most common use-case is in ldapjs filters, which take a string)</p>
</dd>
<dt><a href="#parseIntegerDate">parseIntegerDate(buffer)</a> ⇒ <code>Date</code></dt>
<dd><p>Parse an LDAP IntegerDate buffer into a javascript Date object</p>
</dd>
<dt><a href="#toIntegerDateString">toIntegerDateString(date)</a> ⇒ <code>String</code></dt>
<dd><p>Convert a javascript Date object into an LDAP IntegerDate string
(Can be converted to a buffer with Buffer.from(str, &#39;utf8) but the most common use-case is in ldapjs filters, which take a string)</p>
</dd>
<dt><a href="#parseLargeInteger">parseLargeInteger(buffer, NumberType)</a> ⇒ <code>Date</code></dt>
<dd><p>Parse an LDAP IntegerDate buffer into a javascript Date object</p>
</dd>
<dt><a href="#parseObjectSID">parseObjectSID(buffer)</a> ⇒ <code>String</code></dt>
<dd><p>Parse an LDAP ObjectSID buffer into a string</p>
</dd>
<dt><a href="#toObjectSIDBuffer">toObjectSIDBuffer(str)</a> ⇒ <code>Buffer</code></dt>
<dd><p>Convert an objectSID string into an LDAP ObjectSID buffer</p>
</dd>
<dt><a href="#parseObjectGUID">parseObjectGUID(buffer)</a> ⇒ <code>String</code></dt>
<dd><p>Parse an LDAP ObjectGUID buffer into a string</p>
</dd>
<dt><a href="#toObjectGUIDBuffer">toObjectGUIDBuffer(str)</a> ⇒ <code>Buffer</code></dt>
<dd><p>Convert an ObjectGUID string into an LDAP ObjectGUID buffer</p>
</dd>
<dt><a href="#parseBoolean">parseBoolean(buffer)</a> ⇒ <code>Boolean</code></dt>
<dd><p>Parse an LDAP Boolean buffer into a javascript Boolean</p>
</dd>
<dt><a href="#toBooleanString">toBooleanString(buffer)</a> ⇒ <code>String</code></dt>
<dd><p>Convert a javascript Boolean into a string representation of an LDAP Boolean</p>
</dd>
<dt><a href="#parseUnicodeString">parseUnicodeString(buffer)</a> ⇒ <code>String</code></dt>
<dd><p>Parse an LDAP UnicodeString buffer into a javascript String</p>
</dd>
<dt><a href="#parseUserAccountControl">parseUserAccountControl(buffer)</a> ⇒ <code>Object</code></dt>
<dd><p>Parse an LDAP UserAccountControl buffer into a javascript Object</p>
</dd>
<dt><a href="#parseSAMAccountType">parseSAMAccountType(buffer)</a> ⇒ <code>Object</code></dt>
<dd><p>Parse an LDAP SAMAccountType buffer into a javascript Object</p>
</dd>
</dl>

<a name="coerceEntry"></a>

## coerceEntry(entry) ⇒ <code>Object</code>
Coerce an ldapjs entry into an object with keys (using entry.object as a base)
(This uses module.attributeTransforms to map attribute names to type parsers. Many of the names present are Active-Directory-LDAP specific and I don't honestly know how well it works on other instances)

**Kind**: global function  
**Returns**: <code>Object</code> - An object in the form { sAMAccountName: 'PickleRick', objectSid: 'S-1-5-21-3231561394-915883793-4011936924-6446' }  

| Param | Type | Description |
| --- | --- | --- |
| entry | <code>Object</code> | An entry object retuend from ldapjs |

<a name="parseInteger8"></a>

## parseInteger8(buffer) ⇒ <code>Number</code>
Parse an LDAP Interger8 buffer into a Number

**Kind**: global function  
**Returns**: <code>Number</code> - A Number  

| Param | Type | Description |
| --- | --- | --- |
| buffer | <code>Buffer</code> | A buffer representing an attribute of type Integer8. |

<a name="bufferToPEMCertString"></a>

## bufferToPEMCertString(buffer) ⇒ <code>String</code>
Parse an LDAP OctetString buffer into a base 64 encoded certificate string with line breaks every 64 characters.
(this should match the content in a .pem certificate file)

**Kind**: global function  
**Returns**: <code>String</code> - A Base64 string  

| Param | Type | Description |
| --- | --- | --- |
| buffer | <code>Buffer</code> | A buffer representing an LDAP attribute of type OctetString |

<a name="PEMCertStringToBuffer"></a>

## PEMCertStringToBuffer(str) ⇒ <code>Buffer</code>
Parse a base 64 encoded certificate string into a buffer representing an LDAP OctetString.

**Kind**: global function  
**Returns**: <code>Buffer</code> - A buffer representing an LDAP attribute of type OctetString.  

| Param | Type | Description |
| --- | --- | --- |
| str | <code>String</code> | A Base64 string |

<a name="parseGeneralizedDate"></a>

## parseGeneralizedDate(buffer) ⇒ <code>Date</code>
Parse an LDAP GeneralizedDate buffer into a javascript Date object

**Kind**: global function  
**Returns**: <code>Date</code> - A Date object  

| Param | Type | Description |
| --- | --- | --- |
| buffer | <code>Buffer</code> | A buffer representing an LDAP GeneralizedDate attribute. |

<a name="toGeneralizedDateString"></a>

## toGeneralizedDateString(date) ⇒ <code>String</code>
Convert a javascript Date object into an LDAP GeneralizedDate string
(Can be converted to a buffer with Buffer.from(str, 'utf8) but the most common use-case is in ldapjs filters, which take a string)

**Kind**: global function  
**Returns**: <code>String</code> - - A String in the format "YYYYMMDDHHMMSS.mmmZ" eg."20211225070000.000Z"  

| Param | Type | Description |
| --- | --- | --- |
| date | <code>Date</code> | A Date object |

<a name="parseIntegerDate"></a>

## parseIntegerDate(buffer) ⇒ <code>Date</code>
Parse an LDAP IntegerDate buffer into a javascript Date object

**Kind**: global function  
**Returns**: <code>Date</code> - A Date object  

| Param | Type | Description |
| --- | --- | --- |
| buffer | <code>Buffer</code> | A buffer representing an LDAP IntegerDate attribute. |

<a name="toIntegerDateString"></a>

## toIntegerDateString(date) ⇒ <code>String</code>
Convert a javascript Date object into an LDAP IntegerDate string
(Can be converted to a buffer with Buffer.from(str, 'utf8) but the most common use-case is in ldapjs filters, which take a string)

**Kind**: global function  
**Returns**: <code>String</code> - - A string representation of an integer  

| Param | Type | Description |
| --- | --- | --- |
| date | <code>Date</code> | A Date object |

<a name="parseLargeInteger"></a>

## parseLargeInteger(buffer, NumberType) ⇒ <code>Date</code>
Parse an LDAP IntegerDate buffer into a javascript Date object

**Kind**: global function  
**Returns**: <code>Date</code> - A Date object  

| Param | Type | Description |
| --- | --- | --- |
| buffer | <code>Buffer</code> | A buffer representing an LDAP IntegerDate attribute. |
| NumberType | <code>function</code> | A constructor for the Type of Number you want returned (defaults to String). You can supply "BigInt" here, if your environment supports it. |

<a name="parseObjectSID"></a>

## parseObjectSID(buffer) ⇒ <code>String</code>
Parse an LDAP ObjectSID buffer into a string

**Kind**: global function  
**Returns**: <code>String</code> - An ObjectSID string eg S-1-5-21-3231561394-915883793-4011936924-6446  

| Param | Type | Description |
| --- | --- | --- |
| buffer | <code>Buffer</code> | A buffer representing an LDAP ObjectSID attribute. |

<a name="toObjectSIDBuffer"></a>

## toObjectSIDBuffer(str) ⇒ <code>Buffer</code>
Convert an objectSID string into an LDAP ObjectSID buffer

**Kind**: global function  
**Returns**: <code>Buffer</code> - A buffer representing an LDAP ObjectSID  

| Param | Type | Description |
| --- | --- | --- |
| str | <code>String</code> | An ObjectSID string eg. S-1-5-21-3231561394-915883793-4011936924-6446 |

<a name="parseObjectGUID"></a>

## parseObjectGUID(buffer) ⇒ <code>String</code>
Parse an LDAP ObjectGUID buffer into a string

**Kind**: global function  
**Returns**: <code>String</code> - An ObjectSID string eg S-1-5-21-3231561394-915883793-4011936924-6446  

| Param | Type | Description |
| --- | --- | --- |
| buffer | <code>Buffer</code> | A buffer representing an LDAP ObjectGUID attribute. |

<a name="toObjectGUIDBuffer"></a>

## toObjectGUIDBuffer(str) ⇒ <code>Buffer</code>
Convert an ObjectGUID string into an LDAP ObjectGUID buffer

**Kind**: global function  
**Returns**: <code>Buffer</code> - A buffer representing an LDAP ObjectGUID attribute  

| Param | Type | Description |
| --- | --- | --- |
| str | <code>String</code> | An ObjectGUID string eg. 1E633E26-741B-4D0E-94C7-64D8DED0791F |

<a name="parseBoolean"></a>

## parseBoolean(buffer) ⇒ <code>Boolean</code>
Parse an LDAP Boolean buffer into a javascript Boolean

**Kind**: global function  
**Returns**: <code>Boolean</code> - A javascript Boolean  

| Param | Type | Description |
| --- | --- | --- |
| buffer | <code>Buffer</code> | A buffer representing an LDAP Boolean attribute. |

<a name="toBooleanString"></a>

## toBooleanString(buffer) ⇒ <code>String</code>
Convert a javascript Boolean into a string representation of an LDAP Boolean

**Kind**: global function  
**Returns**: <code>String</code> - An upper case javascript string. Either "TRUE" or "FALSE"  

| Param | Type | Description |
| --- | --- | --- |
| buffer | <code>Boolean</code> | A buffer representing an LDAP Boolean attribute |

<a name="parseUnicodeString"></a>

## parseUnicodeString(buffer) ⇒ <code>String</code>
Parse an LDAP UnicodeString buffer into a javascript String

**Kind**: global function  
**Returns**: <code>String</code> - A javascript String  

| Param | Type | Description |
| --- | --- | --- |
| buffer | <code>Buffer</code> | A buffer representing an LDAP UnicodeString attribute |

<a name="parseUserAccountControl"></a>

## parseUserAccountControl(buffer) ⇒ <code>Object</code>
Parse an LDAP UserAccountControl buffer into a javascript Object

**Kind**: global function  
**Returns**: <code>Object</code> - A javascript Object with Boolean properties representing each possible flag in a UserAccountControl bitmask eg: { accountDisabled: true, lockedOut: false, ...}  

| Param | Type | Description |
| --- | --- | --- |
| buffer | <code>Buffer</code> | A buffer representing an LDAP UserAccountControl attribute |

<a name="parseSAMAccountType"></a>

## parseSAMAccountType(buffer) ⇒ <code>Object</code>
Parse an LDAP SAMAccountType buffer into a javascript Object

**Kind**: global function  
**Returns**: <code>Object</code> - A javascript Object with Boolean properties representing each possible flag in a SAMAccountType bitmask eg: { userObject: true, normalUserAccount: true, ...}  

| Param | Type | Description |
| --- | --- | --- |
| buffer | <code>Buffer</code> | A buffer representing an LDAP SAMAccountType attribute |

